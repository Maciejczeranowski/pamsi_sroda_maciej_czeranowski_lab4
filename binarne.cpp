 //Maciej Czeranowski 226313

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <list>
#include <iterator>

using namespace std; 

//Program wykonany wedlug dodatkowych materialow zamieszczonych na stronie dr inz. �ukasza Jelenia.
 class Position;
  typedef int Elem;					// podstawowy typ elementu
     typedef list<Position> PositionList;		// deklaracja typu PositionList

  typedef int Elem;					// podstawowy typ elementu

    struct Node {					// podstawowy wezel drzewa
      Elem    elt;					// wartosc elementu
      Node*   par;					// wskaznik na ojca
      Node*   left;					// wskaznik na lewego syna
      Node*   right;				// wskaznik na prawego syna
      Node() : elt(), par(NULL), left(NULL), right(NULL) { } // konstruktor wezla
    };

    class Position {					// pozycja w drzewie
    private:
      Node* v;						// wskaznik na wezel na tej pozycji
    public:
      Position(Node* _v = NULL) : v(_v) { }		// konstruktor
      Elem& operator*()					// pobierz element z tej pozycji
        { return v->elt; }
      Position left() const				// pobierz wskaznik na lewego syna od tej pozycji
        { return Position(v->left); }
      Position right() const			// pobierz wskaznik na prawego syna z tej pozycji
        { return Position(v->right); }
      Position parent() const			// pobierz wskaznik na ojca z tej pozycji
        { return Position(v->par); }
      bool isRoot() const				// czy wezel jest korzeniem
        { return v->par == NULL; }
      bool isExternal() const			// czy wezel jest lisciem
        { return v->left == NULL && v->right == NULL; }
      friend class LinkedBinaryTree;	// zaprzyjaznienie z klasa LinkedBinaryTree
    };



  class LinkedBinaryTree {
  protected:
	Node wezel;
  public:
    Position Pozycja;
  public:
    LinkedBinaryTree();					// konstructor klasy
    int size() const;					// ilosc wezlow w drzewie
    bool empty() const;					// czy drzewo jest puste
    Position root() const;				// pobierz pozycje korzenia
    PositionList positions() const;  	// lista pozycji wezlow
    void addRoot();						// dodaj korzen do pustego drzewa
    void expandExternal(const Position& p);		// dodaj liscie do drzewa
    Position removeAboveExternal(const Position& p);	// usuwa wezel oraz jego rodzica
	// i ustawia drugiego syna na rodzica
	Elem Root(); //zwraca wartosc w korzeniu
	int depth(const Position& p); //poziom pozycji
	int height(); //zwraca wysokosc drzewa
  protected: 						
    void preorder(Node* v, PositionList& pl) const;	//dodaje wszystkie wezly do PositionList
  private:
    Node* _root;					// wskaznik na korzen
    int n;						// ilosc wezlow w drzewie
  };
  
    LinkedBinaryTree::LinkedBinaryTree()			// konstructor
    : _root(NULL), n(0) { };
  int LinkedBinaryTree::size() const			// rozmiar drzewa
    { return n; };
  bool LinkedBinaryTree::empty() const			// czy drzewo jest puste
    { return size() == 0; };
  Position LinkedBinaryTree::root() const 		//pobierz wskaznik na korzen
    { return Position(_root); };
  void LinkedBinaryTree::addRoot()				//dodaj korzen do pustego drzewa
    { if(empty())
	 {_root = new Node; n = 1;}
	 else
	 cout<<"drzewo juz ma korzen"; };
  
  void LinkedBinaryTree::expandExternal(const Position& p) { //dodaj liscie do drzewa
    Node* v = p.v;					// pobierz wskaznik na wezel podanej pozycji
    v->left = new Node;					// dodej lewego syna
    v->left->par = v;					// ustaw wezel na jego ojca
    v->right = new Node;				// dodaj prawego syna
    v->right->par = v;					// ustaw wezel na jego ojca
    n += 2;								// zwieksz ilosc wezlow w drzewie
  };
  
  
PositionList LinkedBinaryTree::positions() const { //lista wszystkich wezlow
  PositionList pl;
  preorder(_root, pl);					// przejdz drzewo pre-order
  return PositionList(pl);				// zwroc liste pozycji wezlow
};
							// przejscie wzdluzne pre-order
void LinkedBinaryTree::preorder(Node* v, PositionList& pl) const {
  pl.push_back(Position(v));				// dodaj wezel do listy
  if (v->left != NULL)					// rekurencyjcnie wywolaj dla lewego poddrzewa
    preorder(v->left, pl);
  if (v->right != NULL)					// rekurencyjcnie wywolaj dla prawego poddrzewa
    preorder(v->right, pl);
};


 Position LinkedBinaryTree::removeAboveExternal(const Position& p) { 	// usuwa wezel oraz jego rodzica 
 //i ustawia drugiego syna na rodzica 
    Node* w = p.v;  Node* v = w->par;			// pobiera wskaznik na wezel oraz na ojca
    Node* sib = (w == v->left ?  v->right : v->left); //jesli prawda to prawy, jesli falsz to lewy
    if (v == _root) {					// jesli ojciec jest korzeniem
      _root = sib;					// to ustaw drugiego syna na korzen
      sib->par = NULL;
    }
    else {
      Node* gpar = v->par;				// wskaznik na dziadka wezla
      if (v == gpar->left) gpar->left = sib; 		// zastap ojca synem
      else gpar->right = sib;
      sib->par = gpar;
    }
    delete w; delete v;					// usun podany wezel i jego ojca
    n -= 2;								// zmniejsz licznik wezlow w drzewie
    return Position(sib);
  };
  
  Elem LinkedBinaryTree::Root(){
  	return _root->elt;
  }
  
  int LinkedBinaryTree::depth(const Position& p){
  	if (p.isRoot())
      return 0;					// korzen ma poziom 0
    else
      return 1 + depth(p.parent());		// 1 + (poziom ojca)
  }
  
  int LinkedBinaryTree::height(){
  	int h = 0;
    PositionList nodes = positions();
	list<Position>::iterator q;	
    for (q = nodes.begin(); q != nodes.end(); ++q) {
      if (q->isExternal())
        h = max(h, depth(*q));		// pobierz maksymalny poziom
    }
    return h;
  }
  
  int main(){
  	LinkedBinaryTree drzewo;
  	Position temp;
  	cout<<"utworzenie korzenia, synow, wypelnienie wartosciami"<<endl;
  	drzewo.addRoot();
  	drzewo.root().operator*()=5;
  	drzewo.expandExternal(drzewo.root());
  	drzewo.root().left().operator*()=8;
  	drzewo.root().right().operator*()=7;
  	cout<<"wysokosc drzewa: "<<drzewo.height()<<endl;
  	cout<<"kolejne poddrzewo"<<endl;
 	temp=drzewo.root().left();
 	drzewo.expandExternal(temp);
 	cout<<"wartosci korzenia i synow: "<<drzewo.Root()<<" "<<drzewo.root().left().operator*()<<" "<<drzewo.root().right().operator*()<<endl;
 	cout<<"wysokosc: "<<drzewo.height()<<endl<<"usuniecie poddrzewa z rodzicem"<<endl;
 	drzewo.removeAboveExternal(temp);
 	cout<<"wysokosc: "<<drzewo.height();
  	return 0;
  }
