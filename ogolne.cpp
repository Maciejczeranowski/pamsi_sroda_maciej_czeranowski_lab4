 //Maciej Czeranowski 226313

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <list>
#include <iterator>

using namespace std; 

//Program wykonany wedlug dodatkowych materialow zamieszczonych na stronie dr inz. �ukasza Jelenia.
 class Position;
  typedef int Elem;					// podstawowy typ elementu
    typedef list<Position> PositionList;		// deklaracja typu PositionList
    
    struct Node {					// podstawowy wezel drzewa
      Elem    elt;					// wartosc elementu
      Node*   par;					// wskaznik na ojca
	  PositionList link;			//link do listy z synami
      Node() : elt(), par(NULL), link() { } // konstruktor wezla
    };

    class Position {					// pozycja w drzewie
    private:
      Node* v;						// wskaznik na wezel na tej pozycji
    public:
      Position(Node* _v = NULL) : v(_v) { }		// konstruktor
      Elem& operator*()					// pobierz element z tej pozycji
        { return v->elt; }
//      Position left() const				// pobierz wskaznik na lewego syna od tej pozycji
//        { return Position(v->left); }
//      Position right() const			// pobierz wskaznik na prawego syna z tej pozycji
//        { return Position(v->right); }
	  PositionList child() const		//pobierz link dolisty z synami
	  	{ return PositionList(v->link); }
      Position parent() const			// pobierz wskaznik na ojca z tej pozycji
        { return Position(v->par); }
      bool isRoot() const				// czy wezel jest korzeniem
        { return v->par == NULL; }
      bool isExternal() const			// czy wezel jest lisciem
        { return v->link.empty();}
      friend class LinkedBinaryTree;	// zaprzyjaznienie z klasa LinkedBinaryTree
    };
  typedef list<Position> PositionList;		// deklaracja typu PositionList


  class LinkedBinaryTree {
  protected:
	Node wezel;
  public:
    Position Pozycja;
  public:
    LinkedBinaryTree();					// konstructor klasy
    int size() const;					// ilosc wezlow w drzewie
    bool empty() const;					// czy drzewo jest puste
    Position root() const;				// pobierz pozycje korzenia
    PositionList positions() const;  	// lista pozycji wezlow
    void addRoot();						// dodaj korzen do pustego drzewa
    void expandExternal(const Position& p,int ile);		// dodaj liscie do drzewa
    Position removeAboveExternal(const Position& p);	// usuwa wezel oraz jego rodzica
	// i ustawia drugiego syna na rodzica
	Elem Root(); //zwraca wartosc w korzeniu
	int height(const Position& p); //zwraca wysokosc drzewa
  protected: 						
    void preorder(Node* v, PositionList& pl) const;	//dodaje wszystkie wezly do PositionList
  private:
    Node* _root;					// wskaznik na korzen
    int n;						// ilosc wezlow w drzewie
  };
  
    LinkedBinaryTree::LinkedBinaryTree()			// konstructor
    : _root(NULL), n(0) { };
  int LinkedBinaryTree::size() const			// rozmiar drzewa
    { return n; };
  bool LinkedBinaryTree::empty() const			// czy drzewo jest puste
    { return size() == 0; };
  Position LinkedBinaryTree::root() const 		//pobierz wskaznik na korzen
    { return Position(_root); };
  void LinkedBinaryTree::addRoot()				//dodaj korzen do pustego drzewa
    { if(empty())
	 {_root = new Node; n = 1;}
	 else
	 cout<<"drzewo juz ma korzen"; };
  
  void LinkedBinaryTree::expandExternal(const Position& p,int ile) { //dodaj okreslona ilosc lisci do drzewa
//    list<Position>::iterator i;
    int q,i;
    Node* v = p.v;					// pobierz wskaznik na wezel podanej pozycji
    q=v->link.size();
	for(i=q ;i<=q+ile;i++){
    v->link.push_back(new Node);				// dodej lewego syna
    v->link.back().parent() = v;					// ustaw wezel na jego ojca
	}	 
    n += ile;								// zwieksz ilosc wezlow w drzewie
  };
  
  
PositionList LinkedBinaryTree::positions() const { //lista wszystkich wezlow
  PositionList pl;
  preorder(_root, pl);					// przejdz drzewo pre-order
  return PositionList(pl);				// zwroc liste pozycji wezlow
};
//							 przejscie wzdluzne pre-order
void LinkedBinaryTree::preorder(Node* v, PositionList& pl) const {
  pl.push_back(Position(v));				// dodaj wezel do listy
  PositionList tmp;
  tmp=v->link;
  	list<Position>::iterator i;	
	for(i=v->link.begin() ;i!=v->link.end();i++){
		preorder(tmp.front().v, pl);
		tmp.pop_front();
		}
};


 Position LinkedBinaryTree::removeAboveExternal(const Position& p) { 	// usuwa wezel
 //i ustawia drugiego syna na rodzica 
    Node* w = p.v; 	Node* v = w->par;		// pobiera wskaznik na wezel oraz na ojca
	if(!w->link.empty())
	cout<<"najpierw usun synow"<<endl;
	else
	{
	delete w;
	}
  }
  
    Elem LinkedBinaryTree::Root(){
  	return _root->elt;
  }
  
  int LinkedBinaryTree::height( const Position& p){
    if (p.isExternal()) return 0;		// leaf has height 0
    int h = 0;
	list<Position>::iterator q;
    PositionList ch = p.child();		// list of children
    for ( q = ch.begin(); q != ch.end(); ++q)
      h = max(h, height(*q));
    return 1 + h;				// 1 + max height of children
  }
  
  int main(){
  	LinkedBinaryTree drzewo;
  	Position temp;
  	cout<<"utworzenie korzenia, synow, wypelnienie wartosciami"<<endl;
  	drzewo.addRoot();
  	drzewo.root().operator*()=5;
  	drzewo.expandExternal(drzewo.root(),2);
  	drzewo.root().child().front().operator*()=8;
  	cout<<"wysokosc drzewa: "<<drzewo.height(drzewo.root())<<endl;
  	cout<<"kolejne poddrzewo"<<endl;
 	temp=drzewo.root().child().front();
 	drzewo.expandExternal(temp,1);
 	cout<<"wartosci korzenia i synow: "<<drzewo.Root()<<endl;
 	cout<<"wysokosc: "<<drzewo.height(drzewo.root())<<endl<<"usuniecie poddrzewa z rodzicem"<<endl;
 	drzewo.removeAboveExternal(temp);
 	cout<<"wysokosc: "<<drzewo.height(drzewo.root());
  	return 0;
  }
