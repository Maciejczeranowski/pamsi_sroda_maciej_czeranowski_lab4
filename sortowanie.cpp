//Maciej Czeranowski 226313

#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

template <typename E>
class Kolejka; //naglowek klasy kolejka

template <typename E>
class Sekwencja; //naglowek klasy sekwencja

template <typename E>
class wezel{ //klasa podstawowa zawierajaca pojedynczy wezel
	private:
		E elem; //element w pojedynczym wezle
		int klucz; //klucz okreslajacy priorytet
		wezel<E>* next; //wskaznik na nastepny wezel
		wezel<E>* prev; //wskaznik na poprzedni
		friend class Kolejka<E>; //zaprzyjaznienie z klasa kolejka
	public:
		E GetElement(){ //funkcje do operowania na prywatnych elementach klasy
			return elem;
		}
		wezel GetNext(){
			return next;
		}
		wezel GetPrev(){
			return prev;
		}
		void SetElement(E newE){
			elem=newE;
		}
		void SetNext(wezel<E> newN){
			next=newN;
		}
		void SetPrev(wezel<E> newN){
			prev=newN;
		}		
};

template <typename E>
class Kolejka {
private:
	wezel<E>* head; //wskaznik na pierwszy wezel
	wezel<E>* tail; //wskaznik na ostatni wezel
	friend class Sekwencja<E>; //zaprzyjaznienie z klasa sekwencja
public:
	Kolejka(); //konstruktor klasy kolejka
	~Kolejka(); //destruktor klasy kolejka
	string KolejkaEmptyException();
	int rozmiar() const; //Zwraca ilo�� obiekt�w przechowywanych w kolejce
	bool isEmpty() const; //Zwraca true je�li kolejka jest pusta
	void Wstaw(const E& obj, int klucz); //wstawia element w odpowiednie miejsce w kolejce
	E UsunMin(); //usuwa element o najmniejszym kluczu i go zwraca
	void showAll(); //pokazuje wszystkie elementy kolejki
	E atIndex(int i) const; //zwraca klucz na pozycji i
	int IndexOf(const E& p) const; //zwraca indeks elementu p
};

template <typename E>
Kolejka<E>::Kolejka()
	:head(NULL){ //konstruktor ustawia wskanik na pierwszy wezel na NULL
	tail=head;
}

template <typename E>
Kolejka<E>::~Kolejka(){ //destruktor 
	if(head!=NULL){
	while(head->next!=NULL){ //usuwanie wszystkich elementow z kolejki
		wezel<E>* old = head;
		head = old->next;
		delete old;	
	}
	wezel<E>* old = head;
	head = NULL; //usuwanie ostatniego wezla i ustawienie heada na NULL
	delete old;
}
}

template <typename E>
string Kolejka<E>::KolejkaEmptyException(){ //wyjatek jak kolejka jest pusta
return "kolejka pusta ";
}

template <typename E>
int Kolejka<E>::rozmiar() const{ //sprawdza rozmiar kolejki
int i;
i=0;
wezel<E>* v = new wezel<E>;
v=head;
while(v!=NULL){
	i++;
	v=v->next;
}
return i;
}

template <typename E>
bool Kolejka<E>::isEmpty() const{ //funkcja zwraca true jesli kolejka jest pusta a false jesli nie
if(head==NULL)
return true;
else
return false;
}

template <typename E>
void Kolejka<E>::Wstaw(const E& obj, int klucz) //wstawia element w odpowiednie miejsce w kolejce
{wezel<E>* v = new wezel<E>;
v->next=NULL;
v->elem=obj;
v->klucz=klucz;
v->prev=NULL;
if(isEmpty()){
	head=v;
	tail=v;
}
else{
	if(head->klucz>=klucz){
		head->prev=v;
		v->next=head;
		head=v;
	}
	else{
	wezel<E>* r = new wezel<E>;	
	r=head;
	while(r->next!=NULL && r->next->klucz<klucz){
		r=r->next;
	}
	if(v->next!=NULL){
	v->prev=r;
	v->next=r->next;
	r->next->prev=v;
	r->next=v;
}
else{
	v->prev=r;
	v->next=r->next;
	r->next=v;
	tail=v;
	}
}}
}

template <typename E>
E Kolejka<E>::UsunMin(){
	E temp;
	if(head==NULL){
		return 0;
	}
	else{
		wezel<E>* r = new wezel<E>;	
		r=head;
		head=head->next;
		head->next->prev=NULL;
		if(head==NULL){
			tail==NULL;
		}
		temp=r->elem;
		delete r;
	return temp;
	}
}

template <typename E>
void Kolejka<E>::showAll(){
	wezel<E>* v = new wezel<E>;
	v = head;
	while(v!=NULL){ //wyswietlenie wszystkich elementow z kolejki na ekran
		cout<<v->elem<<":"<<v->klucz<<endl;
		v = v->next;
	}
	delete v;
}

template <typename E> //zmodyfikowane na potrzeby sortowania by zwracalo klucz a nie element
E Kolejka<E>::atIndex(int i) const{
	int k;
	wezel<E>* v = new wezel<E>;	
	v=head;
	for(k=0;k<i;k++){
		v=v->next;
	}
	return v->klucz;
}

template <typename E>
int Kolejka<E>::IndexOf(const E& p) const{
	int k=0;
	wezel<E>* v = new wezel<E>;	
	v=head;
	while(v->elem!=p){
		v=v->next;
		k++;
	}
	return k;
}

int main(){
	Kolejka<int> nowakolejka;
	cout<<"Sortowanie tablicy dla 10 elementow"<<endl<<"przed posortowaniem"<<endl;
	int tab[10],i;
	for (i=0;i<10;i++){
		tab[i]=rand() % 50;
		cout<<tab[i]<<"  ";
	}
	cout<<endl<<"wstawianie do kolejki priorytetowej"<<endl;
	for(i=0;i<10;i++){
		nowakolejka.Wstaw(0,tab[i]);
	}
	for(i=0;i<10;i++){
		tab[i]=nowakolejka.atIndex(i);
	}
	cout<<"posortowana tablica"<<endl;
	for (i=0;i<10;i++){
		cout<<tab[i]<<"  ";
	}
	
}
